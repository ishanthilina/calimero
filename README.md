Calimero
=========

Use the following syntax format to run the built in profiles.

>**mvn** target **-P**profile-name

Ex :-

>mvn package -Prun-gui

where target is **package** and the profile name is **run-gui**.

The following profiles are supported.

- **run-gui** : Runs the main class in the gui project for every target after the *package* phase.
- **run-proc-comm** : Runs the the main method of the ProcComm class with the parameter -h in the tools project for every target after the *package* phase.
- **run-network-monitor** : Runs the the main method of the NetworkMonitor class with the parameter -h in the tools project for every target after the *package* phase.
- **run-dev-info** :  Runs the the main method of the DeviceInfo class with the parameter -h in the tools project for every target after the *package* phase.                                                                                                                                         
